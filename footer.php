	<div class="footer-layout">
		<?php 
			$post   = get_post( 7651 );
			$output =  apply_filters( 'the_content', $post->post_content );
			echo $output;
		?>
	</div>
	<?php if(dh_get_theme_option('footer-area',1)):?>
	<div class="footer-widget">
		<div class="<?php dh_container_class() ?>">
			<div class="footer-widget-wrap">
				<div class="row">
					<?php 
					$area_columns = dh_get_theme_option('footer-area-columns',4);
					if($area_columns == '5'):
						?>
						<?php if(is_active_sidebar('sidebar-footer-1')):?>
						<div class="footer-widget-col col-md-4 col-sm-6">
							<?php dynamic_sidebar('sidebar-footer-1')?>
						</div>
						<?php endif;?>
						<?php if(is_active_sidebar('sidebar-footer-2')):?>
						<div class="footer-widget-col col-md-2 col-sm-6">
							<?php dynamic_sidebar('sidebar-footer-2')?>
						</div>
						<?php endif;?>
						<?php if(is_active_sidebar('sidebar-footer-3')):?>
						<div class="footer-widget-col col-md-2 col-sm-6">
							<?php dynamic_sidebar('sidebar-footer-3')?>
						</div>
						<?php endif;?>
						<?php if(is_active_sidebar('sidebar-footer-4')):?>
						<div class="footer-widget-col col-md-2 col-sm-6">
							<?php dynamic_sidebar('sidebar-footer-4')?>
						</div>
						<?php endif;?>
						<?php if(is_active_sidebar('sidebar-footer-5')):?>
						<div class="footer-widget-col col-md-2 col-sm-6">
							<?php dynamic_sidebar('sidebar-footer-5')?>
						</div>
						<?php endif;?>
						<?php
					else:
					$area_class = '';
						if($area_columns == '2'){
							$area_class = 'col-md-6 col-sm-6';
						}elseif ($area_columns == '3'){
							$area_class = 'col-md-4 col-sm-6';
						}elseif ($area_columns == '4'){
							$area_class = 'col-md-3 col-sm-6';
						}
						?>
						<?php for ( $i = 1; $i <= $area_columns ; $i ++ ) :?>
							<?php if(is_active_sidebar('sidebar-footer-'.$i)):?>
							<div class="footer-widget-col <?php echo esc_attr($area_class) ?>">
								<?php dynamic_sidebar('sidebar-footer-'.$i)?>
							</div>
							<?php endif;?>
						<?php endfor;?>
					<?php endif;?>
				</div>
			</div>
		</div>
	</div>
	<?php endif;?>
	<?php if(dh_get_theme_option('footer-newsletter',0)):?>
	<div class="footer-newsletter">
		<div class="<?php dh_container_class() ?>">
			<div class="footer-newsletter-wrap">
				<div class="row">
					<div class="col-sm-7">
						<h3 class="footer-newsletter-heading"><?php _e('Sign up and be the first to know whats new.',DH_THEME_DOMAIN)?></h3>
					</div>
					<div class="col-sm-5">
						<?php dh_mailchimp_form();?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif;?>
	<footer id="footer" class="footer" itemtype="<?php echo dh_get_protocol()?>://schema.org/WPFooter" itemscope="itemscope" role="contentinfo">
		<div class="footer-info">
			<div class="<?php dh_container_class() ?>">
				<div class="footer-info-wrap">
					<div class="row">
						<?php if($footer_copyright = dh_get_theme_option('footer-copyright')):?>
						<div class="col-sm-6">
							<div class="copyright"><?php echo ($footer_copyright) ?></div>
		            	</div>
		            	<?php endif;?>
		            	<?php if($footer_info = dh_get_theme_option('footer-info')):?>
		            	<div class="col-sm-6">
							<div class="footer-info-text text-right"><?php echo ($footer_info)?></div>
		            	</div>
		            	<?php endif;?>
		            </div>
		         </div>
			</div>
		</div>
	</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>
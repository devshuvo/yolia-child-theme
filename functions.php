<?php
/**
 * Child Theme Functions
 * Add custom code below
 */

function laser_functions_extend(){

	register_post_type('layout',array(
		'labels' => array(
			'name' => __('Layout','abiz'),
			),
		'description' => __('','abiz'),
        'exclude_from_search' => true,	
		'public' => true,
		'menu_position' => 21,
		'menu_icon' => 'dashicons-nametag',
		'supports' => array('title', 'editor'),
	));

}
add_action( 'init', 'laser_functions_extend' );

function laser_theme_setup(){	
	add_image_size( 'custom-portfolio', 410, 380,  true );
}
add_action( 'after_setup_theme', 'laser_theme_setup' );

function extend_laser_scripts(){
	wp_enqueue_script( 'custom-js', get_stylesheet_directory_uri().'/assets/js/custom-script.js', array( 'jquery'), '1.6.6', true );
}
add_action('wp_enqueue_scripts','extend_laser_scripts');